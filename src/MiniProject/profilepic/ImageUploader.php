<?php
namespace App\MiniProject\profilepic;
session_start();

use App\MiniProject\Utility\Utility;
use App\MiniProject\profilepic\Message;
use PDO;
class ImageUploader {
    public $id='';
    public $image='';
    public $profile_name='';
    public $username='root';
    public $password='';
    public $conn='';
    
    public function __construct() {
        try {
            $this->conn=new PDO('mysql:host=localhost;dbname=crudminiproject',$this->username,  $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        } 
        catch (PDOException $e) {
            echo 'ERROR;'.$e->getMessage();
        }
    }
    public function prepare1($data=array()){
        if(is_array($data) && array_key_exists('profile_name', $data)){
            $this->profile_name= $data['profile_name'];
        }
        if(array_key_exists('image', $data) && !empty($data['image'])){
            $this->image= $data['image'];
    }
    if(array_key_exists('id', $data)&& !empty($data['id'])){
        $this->id=$data['id'];
    }
    return $this;
   }
   public function store(){
       if(!empty($this->profile_name)|| !empty($this->image)){
           $stmt="INSERT INTO `profilepic`( `profile_name`, `image`) VALUES (:profile_name,:image)";
           $q=  $this->conn->prepare($stmt);
           $q->execute(array(':profile_name'=>  $this->profile_name,':image'=>  $this->image));
       }
       if($q){
           Message::message("profile data has been inserted successfully");
           Utility::redirect();
       }
       else{
           Message::message('data error');
           header('Location:create.php');
       }
   }
    public function index(){
           $data=array();
           $sql="SELECT * FROM `profilepic`";
           $q=  $this->conn->query($sql) or die('failed!!');
           while ($row=$q->fetch(PDO::FETCH_ASSOC)){
               $data[]=$row;
           }
           return $data;
       }
      public function show(){
          $sql="SELECT * FROM `profilepic` WHERE id=:id";
          $q=  $this->conn->prepare($sql);
          $q->execute(array(':id'=>  $this->id));
          $singledata=$q->fetch(PDO::FETCH_ASSOC);
          return $singledata;
      }
      public function edit(){
          $sql="SELECT * FROM `profilepic` WHERE id=:id";
          $q=  $this->conn->prepare($sql);
          $q->execute(array(':id'=>  $this->id));
          $singledata=$q->fetch(PDO::FETCH_ASSOC);
          return $singledata;
      }
      public function update(){
          if(!empty($this->image)){
              $sql="UPDATE `profilepic` SET `profile_name`=:profile_name,`image`=:image WHERE `profilepoic`.`id`=:id";
               $q=  $this->conn->prepare($sql);
               $q->execute(array(`:profile_name`=>  $this->profile_name,`:image`=>  $this->image,`:id`=>  $this->id));
          }
          else{
               $query="UPDATE `profilepic` SET `profile_name`=:profile_name WHERE `profilepoic`.`id`=:id";
                $q=  $this->conn->prepare($query);
               $q->execute(array(`:profile_name`=>  $this->profile_name,`:id`=>  $this->id)); 
          }
          if($q){
              Message::message('profile data has been updated successfully');
              Utility::redirect();
          }
          else{
              Message::message('Error');
          }
      }
}
