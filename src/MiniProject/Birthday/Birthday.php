<?php
namespace App\MiniProject\Birthday;
session_start();
use App\MiniProject\Birthday\Message;
use PDO;
class Birthday {
    public $id='';
    public $name='';
    public $birthday='';
    public $conn='';
    public $user_name='root';
    public $password='';
    
    public function __construct() {
        try {
            $this->conn=new PDO('mysql:host=localhost;dbname=crudminiproject',  $this->user_name,  $this->password);
             $this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        catch (Exception $e) {
            echo 'ERROR;'.$e->getMessage();
        }
        
}
          public function prepare1($data=array()){
        if(is_array($data) && array_key_exists('name', $data)){
            $this->name= $data['name'];
        }
        if(array_key_exists('birthday', $data) && !empty($data['birthday'])){
            $this->birthday= $data['birthday'];
    }
    if(array_key_exists('id', $data)&& !empty($data['id'])){
        $this->id=$data['id'];
    }
    return $this;
   }
   public function store(){
       if(!empty($this->name)|| !empty($this->birthday)){
           $stmt="INSERT INTO `birthday`( `name`, `birthday`) VALUES (:name,:birthday)";
           $q=  $this->conn->prepare($stmt);
           $q->execute(array(':name'=>  $this->name,':birthday'=>  $this->birthday));
       }
       if($q){
           Message::message("profile data has been inserted successfully");
           header('location:index.php');
       }
       else{
           Message::message('data error');
           header('Location:index.php');
       }
   }
   public function index(){
           $data=array();
           $sql="SELECT * FROM `birthday`";
           $q=  $this->conn->query($sql) or die('failed!!');
           while ($row=$q->fetch(PDO::FETCH_ASSOC)){
               $data[]=$row;
           }
           return $data;
       }
}
