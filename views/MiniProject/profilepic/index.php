<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\profilepic\ImageUploader;
use App\MiniProject\profilepic\Message;
$profile= new ImageUploader();
$all_info=$profile->prepare1()->index();
?>
<a href="create.php">Add again</a>
<html>
    <head>
        <title>
            
        </title>
    </head>
    <body>
        <?php 
           if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
                echo Message::message();
            }
        ?>
        <table border="1">
            <thead>
            <th>SL</th>
            <th>ID</th>
            <th>Full name</th>
            <th>Profile pictures</th>
            <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    $serial=0;
                    foreach ($all_info as $info){
                    $serial++;    
                    
                  ?>
                <tr>
                    <td><?php echo $serial; ?></td>
                    <td><?php echo $info['id'] ?></td>
                    <td><?php echo $info['profile_name'];?></td>
                    <td><img src="<?php echo "../../../image/".$info['image'];?>" alt="No image" height="200" width="200"></td>
                    <td>
                        <a href="show.php?id=<?php echo $info['id']?>">VIEW</a>|
                        <a href="edit.php?id=<?php echo $info['id']?>">EDIT</a>|
                        <a href="delete.php?id=<?php echo $info['id']?>">DELETE</a>                        
                    </td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>