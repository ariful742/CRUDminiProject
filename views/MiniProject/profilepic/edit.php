<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\profilepic\ImageUploader;

$profile=new ImageUploader();
$singledata=$profile->prepare1($_GET)->edit();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>profile Information</title>
    </head>
    <body>
        <h1>Profile Information</h1>
        <fieldset>
            <legend>Add Profile Information</legend>
            <form action="update.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $singledata['id']?>">
                <label>Enter your name</label>
                <input type="text" name="profile_name" value="<?php echo $singledata['profile_name']?>"><br><br>
                 <label>Enter your pictures</label>
                 <input type="file" name="image" value="<?php echo $singledata['image']?>"><br>
                 <img src="<?php echo "../../../image/".$singledata['image']?>" height="200" width="200">
                <input type="submit" value="Update">                
            </form>
        </fieldset>
    </body>
</html>

