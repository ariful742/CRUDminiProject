<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\profilepic\ImageUploader;

$profile=new ImageUploader();
$singledata=$profile->prepare1($_GET)->show();

?>
<html>
    <body>
        <a href="index.php">Back to index</a>
        <h2>ID</h2>
        <ul>
            <li><?php echo $singledata['id']?></li>
        </ul>
        <h2>Profile name</h2>
        <ul>
            <li><?php echo $singledata['profile_name']?></li>
        </ul>
        <h2>Profile Pictures</h2>
        <ul>
            <li><img src="<?php echo "../../../image/".$singledata['image']?>" width="200" height="200"></li>
        </ul>
    </body>
</html>

