<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\profilepic\ImageUploader;

$profile=new ImageUploader();
$singledata=$profile->prepare1($_POST)->show();
if(isset($_FILES['image'])){
    $error=array();
    $file_name=  time().$_FILES['image']['name'];
    $file_type=$_FILES['image']['type'];
    $file_tmp=$_FILES['image']['tmp_name'];
    $file_size=$_FILES['image']['size'];
    $file_ext=  strtolower(end(explode('.', $_FILES['image']['name'])));
    
    $formats=array("jpg","jpeg","png");
    
    if(in_array($file_ext, $formats)=== FALSE){
        $error[]="Extension not allowed";
    }
    if(empty($error)==TRUE){
        unlink($_SERVER['DOCUMENT_ROOT'].'/CRUDMiniProject/image/'.$singledata['image']);
        move_uploaded_file($file_tmp,"../../../image/".$file_name );
        $_POST['image']=$file_name;
    }
    else{
        print_r($error);
    }
}    
$profile->prepare1($_POST)->update();
?>

