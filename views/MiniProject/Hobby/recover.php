<?php
include_once ('../../../vendor/autoload.php');
use App\MiniProject\Hobby\Hobbies;

$hobbies= new Hobbies();
$hobbies->prepare($_GET)->recover();