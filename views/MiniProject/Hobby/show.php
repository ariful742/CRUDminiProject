<?php
include_once ('../../../vendor/autoload.php');

use App\MiniProject\Hobby\Hobbies;
$hobbies= new Hobbies();
$hobby=$hobbies->prepare($_GET)->show();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRUD | Let's Try</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../../css/style.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Fugaz+One' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 
</head>
<body>    
<div class="jumbotron header">
    <h1 class="text-center">CRUD Mini Project</h1> 
   
  </div>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">CRUD</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
          <li><a href="../../../index.php">Home</a></li>
          <li><a href="../Mobile/index.php">Mobile</a></li>
          <li><a href="../Book/book.php">Book</a></li>
        <li><a href="#">Birthday</a></li>
        <li><a href="#">Textarea</a></li>
        <li><a href="#">Email</a></li>
        <li><a href="#">Profile</a></li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Radio<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Home</a></li>
                <li><a href="#">Gender</a></li>
                <li><a href="#">Educational Level</a></li>
            </ul>
        </li>
        <li><a href="#">Terms & Condition</a></li>
        <li class="active"><a href="index.php">Hobby</a></li>
        <li><a href="#">City</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
         <div class="jumbotron">
        <ul class="pager">
            <li class="previous"><a href="index.php"><span class="glyphicon glyphicon-chevron-left"></span>Back to home</a></li>
      <li class="next"><a href="#">Next <span class="glyphicon  glyphicon glyphicon-share-alt"></span></a></li>
    </ul>
       <div class="row text-center">
<div class="box col-md-12">
<div class="box-inner">
<div class="box-header well" data-original-title="">
    <h2 class="heading">
<i class="glyphicon glyphicon glyphicon-apple "></i>
Show Single Hobbies</h2>
</div>
<div class="box-content">
<div class="alert alert-info">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div class="row text-left">
<div class="col-md-8">
<div id="DataTables_Table_0_length" class="dataTables_length">
        <div class="main-content padding input_center">
            <h2 class="heading2">HOBBIES</h2>
            <p><?php echo $hobby->hobby;?></p>       
             <h2 class="heading2">ID</h2>
             <p><?php echo $hobby->id; ?></p>
            </div>
                </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
</html>

