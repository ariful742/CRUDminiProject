<?php
include_once ('../../../vendor/autoload.php');
session_start();
use App\MiniProject\Book\Book;
use App\MiniProject\Book\Message;

$books= new Book();
$book=$books->index();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>show all book list</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../../css/style.css" rel="stylesheet" type="text/css"/>
  <link href='https://fonts.googleapis.com/css?family=Fugaz+One' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 
</head>
<body>    
<div class="jumbotron header">
    <h1 class="text-center">CRUD Mini Project</h1> 
   
  </div>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">CRUD</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
          <li><a href="../../../index.php">Home</a></li>
          <li><a href="../Mobile/index.php">Mobile</a></li>
        <li class="active"><a href="book.php">Book</a></li>
        <li><a href="#">Birthday</a></li>
        <li><a href="#">Textarea</a></li>
        <li><a href="../Email/index.php">Email</a></li>
        <li><a href="#">Profile</a></li>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Radio<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="#">Home</a></li>
                <li><a href="#">Gender</a></li>
                <li><a href="#">Educational Level</a></li>
            </ul>
        </li>
        <li><a href="#">Terms & Condition</a></li>
        <li><a href="../Hobby/index.php">Hobby</a></li>
        <li><a href="#">City</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
    <div class="container">
         <div class="jumbotron">
        <ul class="pager">
      <li class="previous"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span>Back to home</a></li>
      <li class="next"><a href="#">Next <span class="glyphicon  glyphicon glyphicon-share-alt"></span></a></li>
    </ul>
       <div class="row text-center">
<div class="box col-md-12">
<div class="box-inner">
<div class="box-header well" data-original-title="">
    <h2 class="heading">
<i class="glyphicon glyphicon glyphicon-apple "></i>
Show all Book List</h2>
</div>
<div class="box-content">
<div class="alert alert-info">
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid">
<div class="row text-left">
<div class="col-md-8">
<div id="DataTables_Table_0_length" class="dataTables_length">
<label>
<select name="DataTables_Table_0_length" size="1" aria-controls="DataTables_Table_0">
<option value="10" selected="selected">10</option>
<option value="25">25</option>
<option value="50">50</option>
<option value="100">100</option>
</select>
records per page
</label>
</div>
</div>
    <div class="col-md-4 text-right"><a href="trashed.php"><button type="button" class="btn btn-success"><span class="glyphicon  glyphicon-check "></span>&nbsp;&nbsp; See all Trash</button> </a><a href="create.php"><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp; Add New</button></a></div>
</div>
<table id="DataTables_Table_0" class="table table-striped table-bordered bootstrap-datatable datatable responsive dataTable" aria-describedby="DataTables_Table_0_info">
<thead>
<tr role="row">
<th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 205px;" aria-sort="ascending" aria-label="Username: activate to sort column descending">SL.</th>
<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189px;" aria-label="Date registered: activate to sort column ascending">Title</th>
<th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 386px;" aria-label="Actions: activate to sort column ascending">Actions</th>
</tr>
</thead>
<tbody role="alert" aria-live="polite" aria-relevant="all">
    <?php
                $slno=0;
                foreach ($book as $allbook){
                    $slno++; 
            ?>
<tr class="odd">
<td class=" sorting_1"><?php echo $slno;?></td>
<td class="center "><?php echo $allbook['title']; ?></td>
<td class="center ">
    <a class="btn btn-success" href="show.php?id=<?php echo $allbook['id'];?>">
<i class="glyphicon glyphicon-zoom-in icon-white"></i>
View &nbsp;&nbsp;&nbsp;
</a>
    &nbsp;&nbsp;
    <a class="btn btn-info" href="edit.php?id=<?php echo $allbook['id'];?>">
<i class="glyphicon glyphicon-edit icon-white"></i>
Edit &nbsp;&nbsp;&nbsp;
</a>
    &nbsp;&nbsp;
    <a class="btn btn-danger" href="delete.php?id=<?php echo $allbook['id'];?>">
<i class="glyphicon  glyphicon-remove"></i>
Delete
</a>
    <a class="btn btn-primary" href="trash.php?id=<?php echo $allbook['id'];?>">
<i class="glyphicon glyphicon-trash icon-white"></i>
Trash
</a>
     <input type="hidden" name="id" value="<?php echo $allbook['id']?>"/>
</td>
                <?php }?>
</tbody>
</table>
            </div>
                    </div>
                 </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
</html>

