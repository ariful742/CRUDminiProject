<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\Book\Book;
$arif= new Book();
$arif->prepare($_GET)->show();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Show Email Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <h1><?php echo $arif->title?></h1>
        <dl>
            <dt>ID</dt>
            <dd><?php echo $arif->id; ?></dd>
        </dl>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>
