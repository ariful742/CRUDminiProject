<?php
include_once ("../../../vendor/autoload.php");
use App\MiniProject\textarea\TextArea;

$textarea=new TextArea();
$textarea->prepare($_GET)->show();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Show mobile data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <h1><?php echo $textarea->title;?></h1>
        <dl>
            <dt>ID</dt>
            <dd><?php echo $textarea->id; ?></dd>
            <dt>description</dt>
            <dd><?php echo $textarea->description; ?></dd>
        </dl>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>



