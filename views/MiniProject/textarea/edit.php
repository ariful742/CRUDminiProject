<?php
include_once ("../../../vendor/autoload.php");
use App\MiniProject\textarea\TextArea;

$textarea=new TextArea();
$text=$textarea->prepare($_GET)->edit();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>update|textarea</title>
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Write your text</legend>
                <input type="hidden" name="id" value="<?php echo $text->id; ?>"/>
                <h1>Forum post</h1>
                <table>
                    <tr>
                        <td>Title:</td>
                        <td><input type="text" name="title" value="<?php echo $text->title?>"></td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td><textarea rows="5" cols="40" name="description" value="<?php echo $text->description?>"></textarea></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="Save">  <input type="submit" value="Add&Save"></td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>


