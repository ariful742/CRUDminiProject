<?php
include_once ("../../../vendor/autoload.php");
use App\MiniProject\textarea\TextArea;

$textarea=new TextArea();
$text= $textarea->index();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>List textarea</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    
    <body>
        <h1>List textarea</h1>
        <div>
            <span>search/Filter</span>
            <span>Download as <a href="#">pdf</a> | <a href="#"> XL </a> <a href="create.php">Create New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        
        <table border="1">
            <thead>
                <tr>
                    <th>SL.</th>
                    <th>ID</th>
                    <th>title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $slno=0;
                foreach ($text as $alltext){
                    $slno++;
                    ?>
                <tr>
                    <td><?php echo $slno; ?></td>
                    <td><?php echo $alltext['id'] ?></td>
                    <td><a href="show.php?id=<?php echo $alltext['id'];?>"><?php echo $alltext['title']; ?></a></td>
                     <td><a href="show.php?id=<?php echo $alltext['id'];?>"><?php echo $alltext['description']; ?></a></td>
                    <td>
                        <a href="edit.php?id=<?php echo $alltext['id'] ?>">Edit</a> |
                        <a href="delete.php?id=<?php echo $alltext['id'] ?>">Delete</a> 
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $alltext['id']; ?>">
                            <button class="delete" type="submit"> Delete2</button>
                        </form>
                        | <a href="">Trash</a>/
                    </td>
                </tr>
                
                <?Php
                }
                ?>
            </tbody>
        </table>
    </body>
</html>



