<?php
include_once '../../../vendor/autoload.php';
use App\MiniProject\textarea\TextArea;
use App\MiniProject\Utility\Utility;

$textarea=new TextArea();
$textarea->prepare($_POST)->store();
$obj=new Utility();
